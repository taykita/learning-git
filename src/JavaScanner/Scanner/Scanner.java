package JavaScanner.Scanner;

public interface Scanner {
    Lex scan();
}
