package JavaScanner.Scanner;

import JavaScanner.Text;

import static java.lang.Character.*;

public class NameScanner implements Scanner{
    public NameScanner(Text text) {
        this.t = text;
    }

    private final DictionaryJavaLex keyWords = new DictionaryJavaLex();

    Text t;

    private String getName() {
        StringBuilder name = new StringBuilder("" + t.getCh());

        t.setNextCh();
        while (isJavaIdentifierPart(t.getCh())) {
            name.append(t.getCh());
            t.setNextCh();
        }

        return name.toString();
    }

    @Override
    public Lex scan() {
        if (isJavaIdentifierStart(t.getCh())) {
            Lex nameLex = Lex.NAME;

            String name = getName();
            if (keyWords.containsKey(name)) {
                nameLex = keyWords.get(name);
            }

            return nameLex;
        } else {
            return Lex.NONE;
        }
    }
}
