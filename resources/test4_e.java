// A decimal numeral is either the single ASCII digit 0, representing the integer zero, 
// or consists of an ASCII digit from 1 to 9 optionally followed by one or more ASCII digits 
// from 0 to 9 interspersed with underscores, representing a positive integer.
444
555
123_
gdfg \ gdfgt
_1


